# MakeVideo


Hello, This is how-to-wiki for MakeVideo. There are two parts to this wiki. 
First is how to use the python file and second is how to use the exe file.


(1) Using Anaconda: 
		-> Download Anaconda : https://www.anaconda.com/download/
		-> Install OpenCV and CSV library using following Commands
				conda install -c conda-forge opencv
				conda install -c anaconda csvkit 

		-> Open Anaconda Prompt
		-> Change to the directory with MakeVideo.py
		-> Follow the instructions from Running the file.

(1) How to use Python file:
	Prerequisite: 
		Python 3.6
		OpenCV python library
		Matplotlib library
		csv Libraray

	Downloading Prerequisites:

		Follow the following links:

		Python 3.6 	: https://www.ics.uci.edu/~pattis/common/handouts/pythoneclipsejava/python.html
		OpenCV 		: https://pypi.org/project/opencv-python/
		Matplotlib	: https://matplotlib.org/faq/installing_faq.html

	Running the file:

	(1) Open the file directory where MakeVideo.py is.
	(2) Make sure the data file (csv) and Video file are in that directory.
	(3) Open the terminal in that directory or open the terminal and change directory to the file directory.
	(4) Type the following in command line

	python MakeVideo.py [VIDEOPATH] [DATAPATH] [START TIME] [END TIME] [HEIGHT] [WIDTH] [X] [Y] [R] [G] [B] [OUTPUTFILE] [EFFECT]

	Parameters:

	--> VIDEOPATH 		: File path of the video.

	--> DATAPATH		: File path of the data file. It needs to be a csv file.

	--> START TIME		: Start time of the video. For example, if you want to crop the video from the 5th second, start time will be 5.

	--> END TIME 		: End time of the video. For example, if you want to cut the video from the 20th second of the original video file, it should b
						  20. Remember, it should always be greater than start time.

	--> HEIGHT			: Height of the video you want to set. Maximum size is 486.

	--> WIDTH			: Width of the video you want to set. Maximum size is 864.

	--> X				: The position on X axis from where the video will be placed. Remember, X + Width should be less than 864.

	--> Y 				: The position on Y axis from where the video will be placed. Remember, Y + Height  should be less than 486.

	--> R  				: Red color in the background. Should be less than 255.

	--> G 				: Green color in the background. Should be less than 255.

	--> B 				: Blue color in the background. Should be less than 255.

	--> OUTPUTFILE 		: Name of the output file. Extension should be mentioned (mp4).
	
	--> EFFECT			: Effect you want to apply. The string should be "sketch" or "negative".


(2) How to use the Exe file:
	
	--> Open the build/exe.win-amd64-3.6 folder.
	--> Run the following command in command line.

	MakeVideo.exe [VIDEOPATH] [DATAPATH] [START TIME] [END TIME] [HEIGHT] [WIDTH] [X] [Y] [R] [G] [B] [OUTPUTFILE] [EFFECT]


	Parameters:

	--> VIDEOPATH 		: File path of the video.

	--> DATAPATH		: File path of the data file. It needs to be a csv file.

	--> START TIME		: Start time of the video. For example, if you want to crop the video from the 5th second, start time will be 5.

	--> END TIME 		: End time of the video. For example, if you want to cut the video from the 20th second of the original video file, it should b
						  20. Remember, it should always be greater than start time.

	--> HEIGHT			: Height of the video you want to set. Maximum size is 486.

	--> WIDTH			: Width of the video you want to set. Maximum size is 864.

	--> X				: The position on X axis from where the video will be placed. Remember, X + Width should be less than 864.

	--> Y 				: The position on Y axis from where the video will be placed. Remember, Y + Height  should be less than 486.

	--> R  				: Red color in the background. Should be less than 255.

	--> G 				: Green color in the background. Should be less than 255.

	--> B 				: Blue color in the background. Should be less than 255.

	--> OUTPUTFILE 		: Name of the output file. Extension should be mentioned (mp4).
	
	--> EFFECT			: Effect you want to apply. The string should be "sketch" or "negative" or "blackandwhite" or "enhance" or "none".
						  Note: Light background enhances the video lights. It is better to record video in normal lighting.

	Note: If the file name has spaces, use " " to type the name. For eg, "First Second"

