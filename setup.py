from cx_Freeze import setup, Executable
import os.path
PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

base = None  

executables = [Executable("MakeVideo.py", base=base)]

packages = ["idna", "sys", "matplotlib", "numpy", "csv", "cv2"]

options = {
    'build_exe': {    
        'packages':packages,
        'include_files':[
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
         ]
    },    
}

setup(
    name = "final",
    options = options,
    version = "1.1",
    description = 'description',
    executables = executables
)