# -*- coding: utf-8 -*-
"""
Created on Fri May 25 14:15:17 2018

@author: ishan
"""

import sys
import  matplotlib.pyplot as plt
import numpy as np
import  csv
import cv2

def main():         
    if(len(sys.argv) == 14):
        video_path = sys.argv[1]
        data_path = sys.argv[2]
        start_time = int(sys.argv[3])
        end_time = int(sys.argv[4])
        HEIGHT = int(sys.argv[5])
        WIDTH = int(sys.argv[6])
        X = int(sys.argv[7])
        Y = int(sys.argv[8])
        B = int(sys.argv[9])
        G = int(sys.argv[10])
        R = int(sys.argv[11])
        output_file = sys.argv[12]
        generateVideoWithCropping(video_path, data_path,start_time, end_time,
                                  output_file, HEIGHT, WIDTH,X,Y,R,G,B)
        return
    
    else:
        print ("Invalid number of Inputs. Exiting the function")
        return
    
# Generate video without cropping or zooming in! Maintains the aspect ratio
# of original video file.
def generateVideoWithoutCropping(video_path, data_path, start_time,
                                 end_time, output_file):
    
    # Read data from each column
    data_X, data_Y, data_Z = readData(data_path)
    # Current data in the CSV.
    current = 0
    # Current Frame number (60 FPS video file)
    frame_int = 0
    
    # Range to set the length of graph, i.e. number of data to be displayed
    # at any moment in the video.
    start = 0
    end = 1000
    
    # Variables for video opening and saving
    cap = cv2.VideoCapture(video_path)
    fourcc = cv2.VideoWriter_fourcc(*'AVC1')
    out = cv2.VideoWriter(output_file,fourcc, 
                          30, (864, 1080))
    
    
    while(cap.isOpened()):
        ret,frame = cap.read()
        # 80 readings and 30 frames per second, avg 2.67 of new data every
        # frame.
        if(frame_int % 3 is 0):
            add = 2
        else:
            add = 3            
        if (continueWriting(ret, frame_int, start_time, end_time)):            
            frame = resizeFrame(frame)
            if(current < len(data_X)):
                img = plotGraph(data_X, data_Y, data_Z, current, start, end)          
            frame[440:800,0:864] = img
            out.write(frame)
        if frame_int > end_time * 30:
            print ('done!')
            break
        frame_int = frame_int + 1
        if frame_int > start_time * 30:    
            current = current + add
        if(current > end - 50):
            start = start + add
            end = end + add
        print(frame_int)
        if cv2.waitKey(33)== 27:
            break
    out.release()
    
    cv2.destroyAllWindows()
    return

def generateVideoWithCropping(video_path, data_path,start_time, end_time,
                              output_file, HEIGHT, WIDTH,X,Y,R,G,B):
    # Read data from each column
    data_X, data_Y, data_Z = readData(data_path)
    # Current data in the CSV.
    current = 0
    # Current Frame number (60 FPS video file)
    frame_int = 0
    
    # Range to set the length of graph, i.e. number of data to be displayed
    # at any moment in the video.
    start = 0
    end = 1000
    
    # Variables for video opening and saving
    cap = cv2.VideoCapture(video_path)
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_file,fourcc, 
                          30, (864, 1080))
    
    while(cap.isOpened()):
        ret,frame = cap.read()
        # 80 readings and 30 frames per second, avg 2.67 of new data every
        # frame.
        if(frame_int % 3 is 0):
            add = 2
        else:
            add = 3            
        if (continueWriting(ret, frame_int, start_time, end_time)):            
            frame = resizeFrame(frame, HEIGHT, WIDTH,X,Y,R,G,B)
            if(current < len(data_X)):
                img = plotGraph(data_X, data_Y, data_Z, current, start, end)          
            frame_height, frame_width = frame.shape[:2]
            frame[int(frame_height/2) + 10:int(frame_height/2) + 370,0:864] = img
            out.write(frame)
        if frame_int > end_time * 30:
            print ('done!')
            break
        frame_int = frame_int + 1
        if frame_int > start_time * 30:    
            current = current + add
        if(current > end - 50):
            start = start + add
            end = end + add
        print(frame_int)
        if cv2.waitKey(33)== 27:
            break
    out.release()
    
    cv2.destroyAllWindows()
    return

def continueWriting(ret, frame_int, start_time, end_time):
    if ret == True:
        if frame_int > start_time * 30 and frame_int < end_time * 30:
            return True
    else:
        return False
    
def resizeFrame(frame, HEIGHT, WIDTH,X,Y,R,G,B):
    frame = cv2.resize(frame, None, fx = 0.45, fy = 1, 
                               interpolation = cv2.INTER_LINEAR)
    frame_height, frame_width = frame.shape[:2]
    RESIZED_FRAME = cv2.resize(frame, None, 
                               fx = WIDTH/frame_width, 
                               fy = HEIGHT/frame_height)
    cv2.rectangle(frame,(0,0),(864,1080),(R,G,B),thickness=cv2.FILLED)
    if(sys.argv[13] == "sketch"):
        RESIZED_FRAME = colorsketch(RESIZED_FRAME)
        
    elif(sys.argv[13] == "negative"):
        RESIZED_FRAME = negative(RESIZED_FRAME)
    elif(sys.argv[13] == "blackandwhite"):
        RESIZED_FRAME = blackandwhite(RESIZED_FRAME)
    elif(sys.argv[13] == "enhance"):
        RESIZED_FRAME = detailenhance(RESIZED_FRAME)
    elif(sys.argv[13] == "edgepreserve"):
        RESIZED_FRAME = edgepreserveandblur(RESIZED_FRAME)
    #RESIZED_FRAME = videoFilter1(RESIZED_FRAME)
    frame[Y:Y + HEIGHT,X:X + WIDTH] = RESIZED_FRAME
    return frame

def readData(data_path):
    data_X = []
    data_Y = []
    data_Z = []
    with open(data_path,'r') as csvfile:
        for x in range(11):
            next(csvfile)
        plots = csv.reader(csvfile,delimiter=',')
        for column in plots:
            data_X.append(float(column[1]))
            data_Y.append(float(column[2]))
            data_Z.append(float(column[3]))
    return data_X, data_Y, data_Z

def plotGraph(data_X, data_Y, data_Z, current, start, end):
    fig = plt.figure(figsize = (10,5))
    plt.plot(data_X[1340:1340+current], color = 'red')
    plt.plot(data_Y[1340:1340+current], color = 'blue')
    plt.plot(data_Z[1340:1340+current], color = 'green')
    ax = plt.subplot(111)
    ax.set_frame_on(False)
    plt.xticks([])
    plt.yticks([])
    #plt.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
    plt.xlim(start,end)
    #plt.ylim(-2,2)
    plt.tight_layout(pad = 2)
    fig.canvas.draw()
    img = np.fromstring(fig.canvas.tostring_rgb(),
                        dtype=np.uint8, sep='')
    img  = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    height, width = img.shape[:2]
    img = cv2.resize(img,None,fx=864/width, fy=360/height, 
                     interpolation = cv2.INTER_CUBIC)
    return img

def videoFilter1(frame):    
    img_rgb = frame 
    numDownSamples = 1
    numBilateralFilters = 7
    img_color = img_rgb
    for _ in range(numDownSamples):
        img_color = cv2.pyrDown(img_color)
    for _ in range(numBilateralFilters):
        img_color = cv2.bilateralFilter(img_color, 9, 9, 7)
    for _ in range(numDownSamples):
        img_color = cv2.pyrUp(img_color)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
    for _ in range(numBilateralFilters):
        img_gray_blur = cv2.bilateralFilter(img_gray, 9, 9, 7)

    img_edge = cv2.adaptiveThreshold(img_gray_blur, 255,
                                     cv2.ADAPTIVE_THRESH_MEAN_C,
                                     cv2.THRESH_BINARY,
                                     blockSize=9,
                                     C=2)
    img_edge = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2RGB)
    height = min(len(img_color), len(img_edge))
    width = min(len(img_color[0]), len(img_edge[0]))
    img_color = img_color[0:height, 0:width]
    img_edge = img_edge[0:height, 0:width]
    final = cv2.bitwise_and(img_color, img_edge)
    
    return final
    

def colorsketch(frame):    
    img_rgb = frame 
    numDownSamples = 1
    numBilateralFilters = 7
    img_color = img_rgb
    for _ in range(numDownSamples):
        img_color = cv2.pyrDown(img_color)
    for _ in range(numBilateralFilters):
        img_color = cv2.bilateralFilter(img_color, 9, 9, 7)
    for _ in range(numDownSamples):
        img_color = cv2.pyrUp(img_color)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
    for _ in range(numBilateralFilters):
        img_gray_blur = cv2.bilateralFilter(img_gray, 9, 9, 7)

    img_edge = cv2.adaptiveThreshold(img_gray_blur, 255,
                                     cv2.ADAPTIVE_THRESH_MEAN_C,
                                     cv2.THRESH_BINARY,
                                     blockSize=9,
                                     C=2)
    img_edge = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2RGB)
    height = min(len(img_color), len(img_edge))
    width = min(len(img_color[0]), len(img_edge[0]))
    img_color = img_color[0:height, 0:width]
    img_edge = img_edge[0:height, 0:width]
    final = cv2.bitwise_and(img_color, img_edge)
    dst_gray, final = cv2.pencilSketch(frame, sigma_s=30, sigma_r=0.05, shade_factor=0.02)
    
    return final


def edgepreserveandblur(frame):    
    img_rgb = frame 
    numDownSamples = 1
    numBilateralFilters = 7
    img_color = img_rgb
    for _ in range(numDownSamples):
        img_color = cv2.pyrDown(img_color)
    for _ in range(numBilateralFilters):
        img_color = cv2.bilateralFilter(img_color, 9, 9, 7)
    for _ in range(numDownSamples):
        img_color = cv2.pyrUp(img_color)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
    for _ in range(numBilateralFilters):
        img_gray_blur = cv2.bilateralFilter(img_gray, 9, 9, 7)

    img_edge = cv2.adaptiveThreshold(img_gray_blur, 255,
                                     cv2.ADAPTIVE_THRESH_MEAN_C,
                                     cv2.THRESH_BINARY,
                                     blockSize=9,
                                     C=2)
    img_edge = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2RGB)
    height = min(len(img_color), len(img_edge))
    width = min(len(img_color[0]), len(img_edge[0]))
    img_color = img_color[0:height, 0:width]
    img_edge = img_edge[0:height, 0:width]
    final = cv2.bitwise_and(img_color, img_edge)
    final = cv2.edgePreservingFilter(final, flags=1, sigma_s=60, sigma_r=0.4)
    final = cv2.medianBlur(final,5)
    
    
    return final

def detailenhance(frame):    
    img_rgb = frame 
    numDownSamples = 1
    numBilateralFilters = 7
    img_color = img_rgb
    for _ in range(numDownSamples):
        img_color = cv2.pyrDown(img_color)
    for _ in range(numBilateralFilters):
        img_color = cv2.bilateralFilter(img_color, 9, 9, 7)
    for _ in range(numDownSamples):
        img_color = cv2.pyrUp(img_color)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
    for _ in range(numBilateralFilters):
        img_gray_blur = cv2.bilateralFilter(img_gray, 9, 9, 7)

    img_edge = cv2.adaptiveThreshold(img_gray_blur, 255,
                                     cv2.ADAPTIVE_THRESH_MEAN_C,
                                     cv2.THRESH_BINARY,
                                     blockSize=9,
                                     C=2)
    img_edge = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2RGB)
    height = min(len(img_color), len(img_edge))
    width = min(len(img_color[0]), len(img_edge[0]))
    img_color = img_color[0:height, 0:width]
    img_edge = img_edge[0:height, 0:width]
    final = cv2.bitwise_and(img_color, img_edge)
    final = cv2.detailEnhance(frame, sigma_s=10, sigma_r=0.15)
    final = cv2.medianBlur(final,5)
    
    return final

def negative(frame):
    img_rgb = frame 
    numDownSamples = 1
    numBilateralFilters = 7
    img_color = img_rgb
    for _ in range(numDownSamples):
        img_color = cv2.pyrDown(img_color)
    for _ in range(numBilateralFilters):
        img_color = cv2.bilateralFilter(img_color, 9, 9, 7)
    for _ in range(numDownSamples):
        img_color = cv2.pyrUp(img_color)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
    for _ in range(numBilateralFilters):
        img_gray_blur = cv2.bilateralFilter(img_gray, 9, 9, 7)

    img_edge = cv2.adaptiveThreshold(img_gray_blur, 255,
                                     cv2.ADAPTIVE_THRESH_MEAN_C,
                                     cv2.THRESH_BINARY,
                                     blockSize=9,
                                     C=2)
    img_edge = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2RGB)
    height = min(len(img_color), len(img_edge))
    width = min(len(img_color[0]), len(img_edge[0]))
    img_color = img_color[0:height, 0:width]
    img_edge = img_edge[0:height, 0:width]
    final = cv2.bitwise_not(frame)
    return final;

def blackandwhite(frame):
    img_rgb = frame
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    img_gray_inv = 255 - img_gray
    img_blur = cv2.GaussianBlur(img_gray_inv, ksize=(21, 21),
                            sigmaX=0, sigmaY=0)
    final = burnV2(img_gray, img_blur)
    final = cv2.cvtColor(final, cv2.COLOR_GRAY2RGB)
    return final

def dodgeV2(image, mask):
  return cv2.divide(image, 255-mask, scale=256)


def burnV2(image, mask):
    img = 255 - cv2.divide(255-image, 255-mask, scale=256)
    return img

main()