import cv2


cap = cv2.VideoCapture("abc.mp4")
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
out = cv2.VideoWriter("filteredVideo.mp4",fourcc, 
                      30, (1920, 1080))
frame_int = 0
while(cap.isOpened()):
    ret,frame = cap.read()
    if ret == True:
        img_rgb = frame 
        numDownSamples = 1
        numBilateralFilters = 7
        img_color = img_rgb
        for _ in range(numDownSamples):
            img_color = cv2.pyrDown(img_color)
        for _ in range(numBilateralFilters):
            img_color = cv2.bilateralFilter(img_color, 9, 9, 7)
        for _ in range(numDownSamples):
            img_color = cv2.pyrUp(img_color)
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
        for _ in range(numBilateralFilters):
            img_gray_blur = cv2.bilateralFilter(img_gray, 9, 9, 7)

        img_edge = cv2.adaptiveThreshold(img_gray_blur, 255,
                                         cv2.ADAPTIVE_THRESH_MEAN_C,
                                         cv2.THRESH_BINARY,
                                         blockSize=9,
                                         C=2)
        img_edge = cv2.cvtColor(img_edge, cv2.COLOR_GRAY2RGB)
        height = min(len(img_color), len(img_edge))
        width = min(len(img_color[0]), len(img_edge[0]))
        img_color = img_color[0:height, 0:width]
        img_edge = img_edge[0:height, 0:width]
        final = cv2.bitwise_and(img_color, img_edge)
        out.write(final)
        if cv2.waitKey(33)== 27:
            break
        print (frame_int)
        frame_int += 1
out.release()
cv2.destroyAllWindows()
