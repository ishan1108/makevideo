3
c 3Z    �               @   s�  d Z ddlZddlZddlZddlmZ ddlmZmZ ddlm	Z	m
Z
 ddlmZ ddlmZ ddlmZ dd	lmZ dd
lmZ ddlmZ ddlmZ ddlmZ ddlmZ ddlmZ ddlmZ ddlmZ ddlmZmZ ddl m!Z! ddl"m#Z# ddl$m%Z% ddl&m'Z' ddl(m)Z) ddl*m+Z+ ej,d� G dd� de-�Z.G dd� de.�Z/d'd!d"�Z0d#d$� Z1ej2d d d fd%d&�Z3dS )(a�  
The API basically only provides one class. You can create a :class:`Script` and
use its methods.

Additionally you can add a debug function with :func:`set_debug_function`.
Alternatively, if you don't need a custom function and are happy with printing
debug messages to stdout, simply call :func:`set_debug_function` without
arguments.

.. warning:: Please, note that Jedi is **not thread safe**.
�    N)�tree)�python_bytes_to_unicode�split_lines)�get_executable_nodes�get_statement_of_position)�debug)�settings)�cache)�classes)�interpreter)�helpers)�
Completion)�	Evaluator)�imports)�usages)�Project)�try_iter_content)�get_module_names�evaluate_call_of_leaf)�dotted_path_in_sys_path)�TreeNameDefinition)�tree_name_to_contexts)�ModuleContext)�
ModuleName)�unpack_tuple_to_dicti�  c               @   st   e Zd ZdZddd�Zejdd� �Zejdd	� �Zd
d� Z	dd� Z
dd� Zddd�Zf fdd�Zdd� Zdd� ZdS )�Scripta�  
    A Script is the base for completions, goto or whatever you want to do with
    |jedi|.

    You can either use the ``source`` parameter or ``path`` to read a file.
    Usually you're going to want to use both of them (in an editor).

    The script might be analyzed in a different ``sys.path`` than |jedi|:

    - if `sys_path` parameter is not ``None``, it will be used as ``sys.path``
      for the script;

    - if `sys_path` parameter is ``None`` and ``VIRTUAL_ENV`` environment
      variable is defined, ``sys.path`` for the specified environment will be
      guessed (see :func:`jedi.evaluate.sys_path.get_venv_path`) and used for
      the script;

    - otherwise ``sys.path`` will match that of |jedi|.

    :param source: The source code of the current file, separated by newlines.
    :type source: str
    :param line: The line to perform actions on (starting with 1).
    :type line: int
    :param column: The column of the cursor (starting with 0).
    :type column: int
    :param path: The path of the file in the file system, or ``''`` if
        it hasn't been saved yet.
    :type path: str or None
    :param encoding: The encoding of ``source``, if it is not a
        ``unicode`` object (default ``'utf-8'``).
    :type encoding: str
    :param source_encoding: The encoding of ``source``, if it is not a
        ``unicode`` object (default ``'utf-8'``).
    :type encoding: str
    :param sys_path: ``sys.path`` to use during analysis of the script
    :type sys_path: list

    N�utf-8c       
      C   s>  || _ |rtjj|�nd | _|d krBt|d��}|j� }W d Q R X t||dd�| _t| j�| _	|d krvt
t| j	�d�n|}d|  k o�t| j	�kn  s�td��t| j	|d  �}|d kr�|n|}d|  ko�|kn  s�td��||f| _|| _tj�  tj�  tj� | _t|d�}	t| j|	�| _|	j| j� tjd	� d S )
N�rb�replace)�errors�   r   z)`line` parameter is not in a valid range.z+`column` parameter is not in a valid range.)�sys_path�init)�
_orig_path�os�path�abspath�open�readr   �_sourcer   �_code_lines�max�len�
ValueError�_pos�_pathr	   �clear_time_cachesr   �
reset_time�parso�load_grammar�_grammarr   r   �
_evaluator�add_script_path�speed)
�self�source�line�columnr%   �encodingr!   �f�line_len�project� r@   �?C:\ProgramData\Anaconda3\lib\site-packages\jedi\api\__init__.py�__init__T   s.    


zScript.__init__c             C   s   | j j| j| jddtjd�S )NFT)�coder%   r	   �
diff_cache�
cache_path)r4   �parser)   r%   r   �cache_directory)r8   r@   r@   rA   �_get_module_nodew   s    zScript._get_module_nodec             C   sL   t | j| j� | j�}| jd k	rHt| jjj| j�}|d k	rHtj| j||� |S )N)	r   r5   rH   r%   r   r?   r!   r   �
add_module)r8   �module�namer@   r@   rA   �_get_module�   s    
zScript._get_modulec             C   s   d| j jt| j�f S )Nz<%s: %s>)�	__class__�__name__�reprr#   )r8   r@   r@   rA   �__repr__�   s    zScript.__repr__c             C   s<   t jd� t| j| j� | j| j| j�}|j� }t jd� |S )a  
        Return :class:`classes.Completion` objects. Those objects contain
        information about the completions, more than just names.

        :return: Completion objects, sorted by name and __ comes last.
        :rtype: list of :class:`classes.Completion`
        zcompletions startzcompletions end)	r   r7   r   r5   rL   r*   r.   �call_signatures�completions)r8   �
completionrR   r@   r@   rA   rR   �   s    

zScript.completionsc                s�   � j � }|j� j�}|dkr4|j� j�}|dkr4g S � jj� j� |�}tj� j||�}dd� |D �}� fdd�|D �}tj	t
|��S )aI  
        Return the definitions of a the path under the cursor.  goto function!
        This follows complicated paths and returns the end, not the first
        definition. The big difference between :meth:`goto_assignments` and
        :meth:`goto_definitions` is that :meth:`goto_assignments` doesn't
        follow imports and statements. Multiple objects may be returned,
        because Python itself is a dynamic language, which means depending on
        an option you can have two different versions of a function.

        :rtype: list of :class:`classes.Definition`
        Nc             S   s   g | ]
}|j �qS r@   )rK   )�.0�sr@   r@   rA   �
<listcomp>�   s    z+Script.goto_definitions.<locals>.<listcomp>c                s   g | ]}t j� j|��qS r@   )r
   �
Definitionr5   )rT   rK   )r8   r@   rA   rV   �   s    )rH   �get_name_of_positionr.   �get_leaf_for_positionr5   �create_contextrL   r   �evaluate_goto_definition�sorted_definitions�set)r8   �module_node�leaf�context�definitions�names�defsr@   )r8   rA   �goto_definitions�   s    zScript.goto_definitionsFc                s�   � fdd�� �j � j�j�}|dkr(g S �jj�j� |�}t�jj||��}|rZdd� }ndd� }� ||�}�fdd�t|�D �}t	j
|�S )	aJ  
        Return the first definition found, while optionally following imports.
        Multiple objects may be returned, because Python itself is a
        dynamic language, which means depending on an option you can have two
        different versions of a function.

        :rtype: list of :class:`classes.Definition`
        c             3   s@   x:| D ]2}||�r2x$� |j � |�D ]
}|V  q"W q|V  qW d S )N)�goto)rb   �checkrK   �result)�filter_follow_importsr@   rA   rh   �   s
    
z6Script.goto_assignments.<locals>.filter_follow_importsNc             S   s   t | t�rdS | jdkS )NFrJ   )�
isinstancer   �api_type)rK   r@   r@   rA   rf   �   s    
z&Script.goto_assignments.<locals>.checkc             S   s   t | tj�S )N)ri   r   �SubModuleName)rK   r@   r@   rA   rf   �   s    c                s   g | ]}t j� j|��qS r@   )r
   rW   r5   )rT   �d)r8   r@   rA   rV   �   s    z+Script.goto_assignments.<locals>.<listcomp>)rH   rX   r.   r5   rZ   rL   �listre   r]   r   r\   )r8   Zfollow_imports�	tree_namer`   rb   rf   rc   r@   )rh   r8   rA   �goto_assignments�   s    	

zScript.goto_assignmentsc                sH   � j � j� j�}|dkrg S tj� j� |�}� fdd�|D �}tj|�S )ag  
        Return :class:`classes.Definition` objects, which contain all
        names that point to the definition of the name under the cursor. This
        is very useful for refactoring (renaming), or to show all usages of a
        variable.

        .. todo:: Implement additional_module_paths

        :rtype: list of :class:`classes.Definition`
        Nc                s   g | ]}t j� j|��qS r@   )r
   rW   r5   )rT   �n)r8   r@   rA   rV   �   s    z!Script.usages.<locals>.<listcomp>)rH   rX   r.   r   rL   r   r\   )r8   Zadditional_module_pathsrn   rb   ra   r@   )r8   rA   r   �   s    zScript.usagesc                sj   t j�j� �j�� � dkrg S �jj�j� � j�}t j�j|� j�j	�j�}t
jd� � �fdd�|D �S )ah  
        Return the function object of the call you're currently in.

        E.g. if the cursor is here::

            abs(# <-- cursor is here

        This would return the ``abs`` function. On the other hand::

            abs()# <-- cursor is here

        This would return an empty list..

        :rtype: list of :class:`classes.CallSignature`
        Nzfunc_call followedc                s4   g | ],}t |d �rtj�j|j� jj� j� j��qS )�
py__call__)	�hasattrr
   �CallSignaturer5   rK   �bracket_leaf�	start_pos�
call_index�keyword_name_str)rT   rl   )�call_signature_detailsr8   r@   rA   rV     s   z*Script.call_signatures.<locals>.<listcomp>)r   �get_call_signature_detailsrH   r.   r5   rZ   rL   rt   �cache_call_signaturesr*   r   r7   )r8   r`   ra   r@   )rx   r8   rA   rQ   �   s    
zScript.call_signaturesc       
         sd  d� j _� j� }|g� j _�z8�x
t|�D � ]�}� j� j|�}|jdkr^t� j ||j	d � n�t
|tj�r�t|j� �}|j� r�|tdd� |j� D ��O }x�|D ]}tj||� q�W nj|jdkr�|j|�}xT|j	d dd� D ]}t|||� q�W n.|jd	k�r� j j||�}n
t||�}t|� � j j�  q*W � fd
d�� j jD �}	tt|	�dd� d�S d� j _X d S )NT�funcdef�classdefr    c             s   s   | ]}|d V  qdS )r    N�����r@   )rT   r%   r@   r@   rA   �	<genexpr>.  s    z#Script._analysis.<locals>.<genexpr>�	expr_stmt�   rK   c                s   g | ]}� j |j kr|�qS r@   )r%   )rT   �a)r8   r@   rA   rV   >  s    z$Script._analysis.<locals>.<listcomp>c             S   s   | j S )N)r:   )�xr@   r@   rA   �<lambda>?  s    z"Script._analysis.<locals>.<lambda>)�keyF)r{   r|   r}   )r5   �is_analysisrH   Zanalysis_modulesr   rL   rZ   �typer   �childrenri   r   �Importr]   �get_defined_names�	is_nested�	get_pathsr   �infer_import�	eval_noder   rd   r   r   �reset_recursion_limitations�analysis�sorted)
r8   r^   �noder`   Zimport_namesrp   �types�testlistrc   Zanar@   )r8   rA   �	_analysis!  s4    





zScript._analysis)NNNNr   N)F)rN   �
__module__�__qualname__�__doc__rB   r	   �memoize_methodrH   rL   rP   rR   rd   ro   r   rQ   r�   r@   r@   r@   rA   r   -   s   & 
"

%(r   c                   s,   e Zd ZdZ� fdd�Z� fdd�Z�  ZS )�Interpretera�  
    Jedi API for Python REPLs.

    In addition to completion of simple attribute access, Jedi
    supports code completion based on static code analysis.
    Jedi can complete attributes of object which is not initialized
    yet.

    >>> from os.path import join
    >>> namespace = locals()
    >>> script = Interpreter('join("").up', [namespace])
    >>> print(script.completions()[0].name)
    upper
    c                sN   ydd� |D �}W n t k
r.   td��Y nX tt| �j|f|� || _dS )a�  
        Parse `source` and mixin interpreted Python objects from `namespaces`.

        :type source: str
        :arg  source: Code to parse.
        :type namespaces: list of dict
        :arg  namespaces: a list of namespace dictionaries such as the one
                          returned by :func:`locals`.

        Other optional arguments are same as the ones for :class:`Script`.
        If `line` and `column` are None, they are assumed be at the end of
        `source`.
        c             S   s   g | ]}t |��qS r@   )�dict)rT   rp   r@   r@   rA   rV   c  s    z(Interpreter.__init__.<locals>.<listcomp>z-namespaces must be a non-empty list of dicts.N)�	Exception�	TypeError�superr�   rB   �
namespaces)r8   r9   r�   �kwds)rM   r@   rA   rB   T  s    zInterpreter.__init__c                s&   t t| �j� }tj| j|| j| jd�S )N)r%   )r�   r�   rH   r   �MixedModuleContextr5   r�   r%   )r8   Zparser_module)rM   r@   rA   rL   j  s    zInterpreter._get_module)rN   r�   r�   r�   rB   rL   �__classcell__r@   r@   )rM   rA   r�   D  s   r�   �utf-8FTc                s\   � �fdd�}t | dd||d���j� ���fdd�t�j� |�D �}tt||�dd	� d
�S )a�  
    Returns a list of `Definition` objects, containing name parts.
    This means you can call ``Definition.goto_assignments()`` and get the
    reference of a name.
    The parameters are the same as in :py:class:`Script`, except or the
    following ones:

    :param all_scopes: If True lists the names of all scopes instead of only
        the module namespace.
    :param definitions: If True lists the names that have been defined by a
        class, function or a statement (``a = b`` returns ``a``).
    :param references: If True lists all the names that are not listed by
        ``definitions=True``. E.g. ``a = b`` returns ``b``.
    c                s   | j jj� }� r|p�o| S )N)�_namern   �is_definition)�_defZis_def)ra   �
referencesr@   rA   �def_ref_filter�  s    znames.<locals>.def_ref_filterr    r   )r:   r;   r%   r<   c                s8   g | ]0}t j�jt� j|jjd kr&|n|j�|���qS )�
file_input)r
   rW   r5   r   rZ   �parentr�   )rT   rK   )�module_context�scriptr@   rA   rV   �  s   znames.<locals>.<listcomp>c             S   s   | j | jfS )N)r:   r;   )r�   r@   r@   rA   r�   �  s    znames.<locals>.<lambda>)r�   )r   rL   r   rH   r�   �filter)r9   r%   r<   �
all_scopesra   r�   r�   rc   r@   )ra   r�   r�   r�   rA   rb   t  s    rb   c              G   s0   x*| D ]"}d| }t |dt|�d�j�  qW dS )z�
    Preloading modules tells Jedi to load a module now, instead of lazy parsing
    of modules. Usful for IDEs, to control which modules to load on startup.

    :param modules: different module names, list of string.
    zimport %s as x; x.r    N)r   r,   rR   )�modules�mrU   r@   r@   rA   �preload_module�  s    
r�   c             C   s   | t _|t _|t _|t _dS )z�
    Define a callback debug function to get all the debug messages.

    If you don't specify any arguments, debug messages will be printed to stdout.

    :param func_cb: The callback function for debug messages, with n params.
    N)r   �debug_function�enable_warning�enable_notice�enable_speed)Zfunc_cb�warnings�noticesr7   r@   r@   rA   �set_debug_function�  s    	r�   )NNr�   FTF)4r�   r$   �sysr2   �parso.pythonr   r   r   �jedi.parser_utilsr   r   �jedir   r   r	   �jedi.apir
   r   r   �jedi.api.completionr   �jedi.evaluater   r   r   �jedi.evaluate.projectr   �jedi.evaluate.argumentsr   �jedi.evaluate.helpersr   r   �jedi.evaluate.sys_pathr   �jedi.evaluate.filtersr   �jedi.evaluate.syntax_treer   �jedi.evaluate.contextr   �jedi.evaluate.context.moduler   �jedi.evaluate.context.iterabler   �setrecursionlimit�objectr   r�   rb   r�   �print_to_stdoutr�   r@   r@   r@   rA   �<module>   sF   
  0 
"